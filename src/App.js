import {gDevcampReact, percentageStudyingStudent} from "./info"
function App() {
  
  var sPercentStudying = percentageStudyingStudent();
  return (
    <div>
      <h1>{gDevcampReact.title}</h1>
      <img src={gDevcampReact.image} alt = "react logo" width = "500"></img>
      <p>Tỷ lệ sinh viên đang học: {sPercentStudying}%</p>
      <p>{sPercentStudying >= 15 ? "Sinh viên học nhiều": "Sinh viên học ít"}</p>
      <ul>
        {gDevcampReact.benefits.map(function(element, index) {
          return <li key={index}>{element}</li>
        })}
      </ul>
    </div>
  );
}

export default App;
